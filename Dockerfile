FROM node:14
WORKDIR /app

COPY package.json package.json
RUN npm install
COPY . .

RUN chown -R node:node /app
RUN chmod 755 /app

USER node

CMD /bin/sh -c "while sleep 86000; do :; done"
