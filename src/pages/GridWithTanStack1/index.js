import React from 'react';
import TanStackGrid from 'components/TanStackGrid';
import data from './data.json';
import { createColumnHelper } from '@tanstack/react-table';

const formatDate = (date) => {
    const [{ value: month }, , { value: day }, , { value: year }] = new Intl.DateTimeFormat('en-US', { dateStyle: 'medium' }).formatToParts(date);
    return `${month} ${day} ${year}`;
};

const GridWithTanStack1 = () => {
    const columns = [
        {
            header: 'Sl.No',
            id: 'slNo',
            accessorKey: 'slNo',
            size: 90,
            enableResizing: false
        },
        {
            header: 'Sender Code',
            accessorKey: 'senderCode',
            enableSorting: false
        },
        {
            header: () => <i>Account Number</i>,
            id: 'accountNumber',
            accessorFn: (row) => row.accountNumber + ' ' + row.senderCode,
            cell: (item) => {
                return <a href='#'>{item.getValue()}</a>;
            },
            maxSize: 220
        },
        {
            header: 'Customer Name',
            id: 'customerName',
            accessorKey: 'customerName'
        },
        {
            header: 'Address',
            id: 'address',
            accessorKey: 'address'
        },
        {
            header: 'City',
            id: 'city',
            accessorKey: 'city'
        },
        {
            header: 'State/Province',
            id: 'stateProvince',
            accessorKey: 'stateProvince'
        },
        {
            header: 'Postal',
            id: 'postal',
            accessorKey: 'postal'
        },
        {
            header: 'Country',
            id: 'country',
            accessorKey: 'country',
            truncateText: true
        },
        {
            header: 'Last Activity',
            id: 'lastActivity',
            accessorKey: 'lastActivity',
            accessorFn: ({ lastActivity }) => formatDate(new Date(lastActivity)),
            sortingFn: 'sortByConvertingToDateObject',
            desc: false
        },
        {
            header: 'Year to date sales',
            id: 'ytdSales',
            accessorKey: 'ytdSales',
            size: 200
        }
    ];

    return <TanStackGrid columns={columns} data={data} stateStoreKey='customer_detail_grid' isLoading={false} />;
};

export default GridWithTanStack1;
