import { useEffect, useState } from 'react';
import { faker } from '@faker-js/faker';

const getFakeData = async () => {
    return new Promise((resolve, reject) => {
        setTimeout(() => {
            resolve(
                new Array(1000).fill().map((e, i) => {
                    return {
                        slNo: i + 1,
                        senderCode: 'ME',
                        accountNumber: faker.random.numeric(8, { allowLeadingZeros: false }),
                        customerName: faker.name.fullName(),
                        address: faker.address.streetAddress(true),
                        city: faker.address.city(),
                        stateProvince: faker.address.state(),
                        postal: faker.address.zipCodeByState(),
                        country: faker.address.country(),
                        lastActivity: faker.date.between('2008-01-01T00:00:00.000Z', '2021-01-01T00:00:00.000Z'),
                        ytdSales: faker.random.numeric(5, { allowLeadingZeros: false })
                    };
                })
            );
        }, 300);
    });
};

const useFakeData = () => {
    const [data, setData] = useState([]);
    const [isLoading, setIsLoading] = useState(true);

    useEffect(() => {
        const getData = async () => {
            setData(await getFakeData());
            setIsLoading(false);
        };
        getData();
    }, []);

    return [data, isLoading];
};

export default useFakeData;
