import React from 'react';
import { BrowserRouter, Routes, Route } from 'react-router-dom';
import Header from 'components/Header';
import GridWithTanStack1 from 'pages/GridWithTanStack1';
import 'App.scss';

const App = () => {
    return (
        <BrowserRouter>
            <Header />
            <Routes>
                <Route path='/tanStackTableDemo1' element={<GridWithTanStack1 />}></Route>
            </Routes>
        </BrowserRouter>
    );
};

export default App;
