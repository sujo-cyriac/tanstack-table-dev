import { Link } from 'react-router-dom';
import { Nav, Navbar } from 'react-bootstrap';

const Header = () => {
    return (
        <Navbar bg='light' expand='lg' className='mb-5'>
            <Navbar.Brand as={Link} to='tanStackTableDemo1'>TanStack-Table demo</Navbar.Brand>
            <Nav className='me-auto'>
                <Nav.Link as={Link} to='tanStackTableDemo1'>
                    Demo 1
                </Nav.Link>
            </Nav>
        </Navbar>
    );
};

export default Header;
