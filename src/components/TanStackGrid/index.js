import React, { useState, useEffect, useRef, useCallback } from 'react';
import { Row, Col, Spinner } from 'react-bootstrap';
import { useReactTable, getCoreRowModel, getSortedRowModel, getFilteredRowModel, getPaginationRowModel } from '@tanstack/react-table';
import Grid from './Grid';
import PageSize from './PageSize';
import GlobalFilter from './GlobalFilter';
import Pagination from './Pagination';
import { retrieveGridState, storeGridStateDebounced } from './utils';
import { globalFilterFn } from './utils/filters';
import * as sortingHelpers from './utils/sorting';
import './style.scss';

const TanStackGrid = ({ columns = [], data = [], defaultColumnProps = {}, stateStoreKey = '', isLoading = false, customErrorMessage = '' }) => {
    const storedGridState = retrieveGridState(stateStoreKey);

    const resetPagination = useRef(false);
    const [globalFilterText, setGlobalFilterText] = useState(storedGridState.globalFilter || '');

    //curretly there is no option to set the defult sort direction from coumnDef, but default sor direction can be set through iniatlState, getting the
    // coulumns from columDef and reformatting the desc keyword into iniatlState format.
    const defaultSortingDirections = columns.reduce((accumulator, { id = '', accessorKey = '', desc }) => {
        typeof desc === 'boolean' && accumulator.push({ id: accessorKey || id, desc });
        return accumulator;
    }, []);

    const tableInstance = useReactTable({
        data,
        defaultColumn: { ...defaultColumnProps },
        columns,
        initialState: { ...{ sorting: defaultSortingDirections }, ...storedGridState },
        state: { globalFilter: globalFilterText },
        columnResizeMode: 'onChange',
        sortingFns: { ...sortingHelpers },
        globalFilterFn,
        getCoreRowModel: getCoreRowModel(),
        getSortedRowModel: getSortedRowModel(),
        getPaginationRowModel: getPaginationRowModel(),
        getFilteredRowModel: getFilteredRowModel()
    });

    const {
        getHeaderGroups,
        getRowModel,
        previousPage,
        nextPage,
        getPageCount,
        getCanNextPage,
        getCanPreviousPage,
        getState,
        getPrePaginationRowModel,
        setPageIndex,
        setPageSize
    } = tableInstance;

    const { pagination = {}, columnSizing = {}, globalFilter = '', sorting = '' } = getState();
    const goToFirstPage = useCallback(() => setPageIndex(0), [setPageIndex]);
    const goToLastPage = useCallback(() => setPageIndex(getPageCount() - 1), [setPageIndex, getPageCount]);

    // // when there is an initialState with pagination.pageIndex afetr when we apply global filter, sort etc, pagination is not reseting to 0
    // to fixing we need to manually set pageIndex to 0 when global filter or sort changes.
    useEffect(() => {
        // making sure that we are not reseting the pagination on first load as we need to load the stored grid state on intail load.
        resetPagination.current && setPageIndex(0);
        resetPagination.current = true;
    }, [globalFilter, sorting, resetPagination, setPageIndex]);

    // storing grid pagination, filter, sort, column size info to session storage when state changes.
    useEffect(() => {
        storeGridStateDebounced(stateStoreKey, { pagination, columnSizing, globalFilter, sorting });
        return () => storeGridStateDebounced.cancel();
    }, [stateStoreKey, pagination, columnSizing, globalFilter, sorting]);

    return isLoading ? (
        <Spinner animation='border' />
    ) : (
        <Row>
            <Col md='6' className='mb-2'>
                <PageSize value={pagination.pageSize} pageSizeSetter={setPageSize} />
            </Col>
            <Col md='6' className='mb-2'>
                <GlobalFilter value={globalFilter} globalFilterSetter={setGlobalFilterText} />
            </Col>
            <Col md='12'>
                <Grid getHeaderGroups={getHeaderGroups} getRowModel={getRowModel} customErrorMessage={customErrorMessage} />
            </Col>
            <Col md='12'>
                <Pagination
                    rowCount={getPrePaginationRowModel().rows.length}
                    pageCount={getPageCount()}
                    pageIndex={pagination.pageIndex}
                    pageSize={pagination.pageSize}
                    hasNextPage={getCanNextPage()}
                    hasPreviousPage={getCanPreviousPage()}
                    onFirstPage={goToFirstPage}
                    onPreviousPage={previousPage}
                    onNextPage={nextPage}
                    onLastPage={goToLastPage}
                />
            </Col>
        </Row>
    );
};

export default TanStackGrid;
