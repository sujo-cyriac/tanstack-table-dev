import debounce from 'lodash/debounce';
import { GRID_STATE_STORE_DEBOUNCE_DELAY_MS } from './constants';

// retrieve table state from session store if there is a key
export const retrieveGridState = (key = '') => (key && JSON.parse(sessionStorage.getItem(key))) || {};

// debounced function stores the grid state into session storage, requires session storage key and data as inputs.
export const storeGridStateDebounced = debounce((key = '', data = {}) => {
    key && sessionStorage.setItem(key, JSON.stringify(data));
}, GRID_STATE_STORE_DEBOUNCE_DELAY_MS);

// calculates the pagination counts base on giving inputs
export const calculatePaging = (pageIndex = 0, pageSize = 0, rowCount = 0) => {
    if (rowCount === 0) return [0, 0, 0];
    const currentPage = pageIndex + 1;
    const fromCount = pageIndex * pageSize + 1;
    const toCount = rowCount / currentPage < pageSize ? rowCount : currentPage * pageSize;
    return [currentPage, fromCount, toCount];
};
