import { sortingFns } from '@tanstack/react-table';
import { compareItems } from '@tanstack/match-sorter-utils';

/**
 * Custom sorting functions adding here will automatiaclly added to grids custom sorting methods, once added the can be used by the column definitions
 * eg:  
 *       columns = [
        {
            header: 'Sl.No',
            id: 'slNo',
            accessorKey: 'slNo',
            sortingFn: 'sortusingRankifAvaiable'
        }
 *   
 */

export const sortusingRankifAvaiable = (rowA, rowB, columnId) => {
    // Only sort by rank if the column has ranking information
    if (rowA.columnFiltersMeta[columnId] && rowB.columnFiltersMeta[columnId])
        return compareItems(rowA.columnFiltersMeta[columnId], rowB.columnFiltersMeta[columnId]);

    // Provide an alphanumeric fallback for when the item ranks are equal
    return sortingFns.alphanumeric(rowA, rowB, columnId);
};

export const sortByConvertingToDateObject = (rowA, rowB, columnId) => {
    const date1 = new Date(rowA.getValue(columnId));
    const date2 = new Date(rowB.getValue(columnId));
    return date1 > date2 ? 1 : date1 < date2 ? -1 : 0;
};
