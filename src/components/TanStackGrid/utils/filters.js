import { rankItem } from '@tanstack/match-sorter-utils';

/**
 * globalFilterFn perform filtering @tanstack/match-sorter-utils are forked from the match-sorter library (https://github.com/kentcdodds/match-sorter)
 */
export const globalFilterFn = (row, columnId, value, addMeta) => {
    //ranking the item

    const itemRank = rankItem(row.getValue(columnId), value, { threshold: 3 });

    // Storing the itemRank info into the grid Row.columnFiltersMeta so that it can be used in future to do sort.
    addMeta(itemRank);
    //if (itemRank.passed) console.log(value, itemRank.passed, row.getValue(columnId))
    // Return if the item should be filtered in/out
    return itemRank.passed;
};
