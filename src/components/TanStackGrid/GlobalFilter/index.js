import React, { useEffect, useState, memo } from 'react';
import { Form, Row, Col } from 'react-bootstrap';
import debounce from 'lodash/debounce';
import { GLOBAL_FILTER_UPDATE_DELAY_MS } from '../utils/constants';

/**
 *
 * Component displays filter in TanStackGrid.
 */
const GlobalFilter = ({ value, globalFilterSetter }) => {
    const [filter, setFilter] = useState(value);

    const debouncedGlobalFilterSetter = debounce((value) => {
        globalFilterSetter(value);
    }, GLOBAL_FILTER_UPDATE_DELAY_MS);

    useEffect(() => {
        debouncedGlobalFilterSetter(filter);
        // cancels the debounce when component unmounts.
        return () => debouncedGlobalFilterSetter.cancel();
    }, [filter, debouncedGlobalFilterSetter]);

    return (
        <Row>
            <Col md='8' className='text-right'>
                <Form.Label>Filter:</Form.Label>
            </Col>
            <Col md='4' className='text-right'>
                <Form.Control
                    md='3'
                    size='sm'
                    type='text'
                    value={filter}
                    onChange={(event) => {
                        setFilter(event.target.value);
                    }}
                />
            </Col>
        </Row>
    );
};

export default memo(GlobalFilter);
