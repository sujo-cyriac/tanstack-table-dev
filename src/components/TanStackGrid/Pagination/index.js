import React, { memo } from 'react';
import { Pagination as Paging } from 'react-bootstrap';
import { BsFillCaretLeftFill, BsFillCaretRightFill } from 'react-icons/bs';
import { IoPlaySkipBackSharp, IoPlaySkipForwardSharp } from 'react-icons/io5';
import { calculatePaging } from '../utils';
import './style.scss';

const Pagination = ({ rowCount, pageCount, pageIndex, pageSize, hasNextPage, hasPreviousPage, onFirstPage, onPreviousPage, onNextPage, onLastPage }) => {
    const [currentPage = 0, fromCount = 0, toCount = 0] = calculatePaging(pageIndex, pageSize, rowCount, pageCount);
    return (
        <Paging className='tanstack-grid-paging'>
            {<Paging.Item className='text'>{`Showing ${fromCount} to ${toCount} of ${rowCount} `}</Paging.Item>}
            <Paging.Item onClick={onFirstPage} disabled={!hasPreviousPage}>
                <IoPlaySkipBackSharp />
            </Paging.Item>
            <Paging.Item onClick={onPreviousPage} disabled={!hasPreviousPage}>
                <BsFillCaretLeftFill />
            </Paging.Item>
            <Paging.Item className='text'>{`Page ${currentPage} of  ${pageCount}`}</Paging.Item>
            <Paging.Item onClick={onNextPage} disabled={!hasNextPage}>
                <BsFillCaretRightFill />
            </Paging.Item>
            <Paging.Item onClick={onLastPage} disabled={!hasNextPage}>
                <IoPlaySkipForwardSharp />
            </Paging.Item>
        </Paging>
    );
};

export default memo(Pagination);
