import React from 'react';
import { Form } from 'react-bootstrap';

/**
 * Allows the user to change number of rows display in TanStack Grid
 */
const PageSize = ({ value, pageSizeSetter }) => {
    return (
        <Form.Control className='w-25' as='select' size='sm' value={value} onChange={(event) => pageSizeSetter(event.target.value)}>
            <option value='10'>10</option>
            <option value='25'>25</option>
            <option value='50'>50</option>
            <option value='100'>100</option>
        </Form.Control>
    );
};

export default React.memo(PageSize);
