import React from 'react';
import { flexRender } from '@tanstack/react-table';

const GridCell = ({ visibleCells = [] }) => (
    <tr>
        {visibleCells.map((cell) => (
            <td key={cell.id}>
                {cell.column.columnDef.truncateText ? (
                    <span title={cell.getValue()} className='truncate-text'>
                        {flexRender(cell.column.columnDef.cell, cell.getContext())}
                    </span>
                ) : (
                    flexRender(cell.column.columnDef.cell, cell.getContext())
                )}
            </td>
        ))}
    </tr>
);

export default GridCell;
