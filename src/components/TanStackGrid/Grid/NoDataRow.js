import React from 'react';

const NoDataRow = ({ message = '', colSpan }) => {
    return (
        <tr>
            <td colSpan={colSpan}>{message ? message : 'No data to display!'}</td>
        </tr>
    );
};

export default NoDataRow;
