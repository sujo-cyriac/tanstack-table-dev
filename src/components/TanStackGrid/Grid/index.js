import React from 'react';
import { Table } from 'react-bootstrap';
import GridHeader from './GridHeader';
import NoDataRow from './NoDataRow';
import GridCell from './GridCell';

// Displays the Grid UI for TanStackGrid
const Grid = ({ getHeaderGroups, getRowModel, customErrorMessage = '' }) => {
    const rows = getRowModel().rows;
    const headerGroups = getHeaderGroups();
    const columnCount = headerGroups.reduce((accumulator, { headers }) => accumulator + headers.length, 0);

    return (
        <div className='grid-wrapper'>
            <Table size='sm' striped bordered hover>
                <thead>
                    <GridHeader headerGroups={headerGroups} />
                </thead>
                <tbody>
                    {customErrorMessage ? (
                        <NoDataRow colSpan={columnCount} message={customErrorMessage} />
                    ) : rows.length > 0 ? (
                        rows.map((row) => <GridCell key={row.id} visibleCells={row.getVisibleCells()} />)
                    ) : (
                        <NoDataRow colSpan={columnCount} />
                    )}
                </tbody>
            </Table>
        </div>
    );
};

export default Grid;
