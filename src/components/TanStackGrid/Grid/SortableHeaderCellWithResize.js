import React from 'react';
import { FaSortUp, FaSort, FaSortDown } from 'react-icons/fa';

const SortableHeaderCellWithResize = ({
    canSort = false,
    canResize = false,
    isResizing = false,
    sortOrder = '',
    toggleSortHandler,
    resizeHandler,
    children
}) => {
    return (
        <>
            {canSort ? (
                <div className='p-1' role='button' onClick={toggleSortHandler()}>
                    {children}
                    <span className='sort-icons'>{sortOrder ? sortOrder === 'asc' ? <FaSortUp /> : <FaSortDown /> : <FaSort />}</span>
                </div>
            ) : (
                children
            )}
            {canResize && <div onMouseDown={resizeHandler()} className={`resizer ${isResizing ? 'isResizing' : ''}`}></div>}
        </>
    );
};

export default SortableHeaderCellWithResize;
