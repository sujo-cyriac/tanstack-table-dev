import React from 'react';
import { flexRender } from '@tanstack/react-table';
import SortableHeaderCellWithResize from './SortableHeaderCellWithResize';

const GridHeader = ({ headerGroups = [] }) =>
    headerGroups.map((headerGroup) => (
        <tr key={headerGroup.id}>
            {headerGroup.headers.map((header) => {
                const sortOrder = header.column.getIsSorted();
                return (
                    <th key={header.id} width={header.getSize()}>
                        {/*console.log(header.column)*/}
                        <SortableHeaderCellWithResize
                            key={'sort' + header.id}
                            canSort={header.column.getCanSort()}
                            canResize={header.column.getCanResize()}
                            sortOrder={sortOrder}
                            isResizing={header.column.getIsResizing()}
                            toggleSortHandler={header.column.getToggleSortingHandler}
                            resizeHandler={header.getResizeHandler}
                        >
                            {!header.isPlaceholder && flexRender(header.column.columnDef.header, header.getContext())}
                        </SortableHeaderCellWithResize>
                    </th>
                );
            })}
        </tr>
    ));

export default GridHeader;
